﻿using BookCatalog.Dtos;
using BookCatalog.Models;
using BookCatalog.Repo;
using Microsoft.AspNetCore.Mvc;
using System;

namespace BookCatalog.Controllers
{
    [ApiController]
    [Route("books")]

    public class BooksController : ControllerBase
    {
        private IBook _bookRepo;
        public BooksController(IBook bookRepo)
        {
            _bookRepo = bookRepo;
        }
        [HttpGet]
        public ActionResult<IEnumerable<BookDTO>> GetBooks()
        {
            return _bookRepo.GetBooks().Select(x => new BookDTO { Id = x.Id, Title = x.Title, Price = x.Price }).ToList();
        }
        [HttpGet("{id}")]
        public ActionResult<BookDTO> GetBook(Guid id)
        {
            var book = _bookRepo.GetBook(id);
            if (book == null)
                return NotFound();

            var bookDTO = new BookDTO { Id = book.Id, Title = book.Title, Price = book.Price };
            return bookDTO;
        }
        [HttpPost]
        public ActionResult CreateBook(CreateOrUpdateBookDTO book)
        {
            var mybook = new Book();
            mybook.Id = Guid.NewGuid();
            mybook.Title = book.Title;
            mybook.Price = book.Price;
            _bookRepo.CreateBook(mybook);
            return Ok();

        }
        [HttpPut("{id}")]
        public ActionResult UpdateBook(Guid id, CreateOrUpdateBookDTO book)
        {
            var mybook = _bookRepo.GetBook(id);
            if(mybook == null)
                return(NotFound());

            mybook.Title = book.Title;
            mybook.Price = book.Price;
            _bookRepo.UpdateBook(id, mybook);
            return Ok();

        }
        [HttpDelete("{id}")]
        public ActionResult DeleteBook(Guid id)
        {
            var book= _bookRepo.GetBook(id);
            if (book == null) NotFound();

            _bookRepo.DeleteBook(id);
            return Ok();
        }
    }
}
